import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Login from './Login';
import AccountList from './AccountList'
import PricelistSearch from './PricelistSearch'


import { BrowserRouter as Router, Route, Link, Redirect } from "react-router-dom";

const Home = () => (
  <div>
    <h2>Home</h2>
  </div>
)

class App extends Component {

  render() {
    return (
      <Router>
        <div>
          <Route path="/login" component={Login}/>
          <Route exact={true} path="/" component={AccountList}/>
          <Route path="/account/:id" component={PricelistSearch}/>
        </div>
      </Router>
    );
  }
}

export default App;
