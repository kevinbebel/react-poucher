import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, Redirect } from "react-router-dom";
import { ListGroup, ListGroupItem, Container } from 'reactstrap';
import { Accounts } from 'rapidtradecore'
import { Form, Input, Button } from 'reactstrap';
class AccountList extends Component {
    constructor() {
        super();
        this.state = {
            accounts: []
        }
        this.user = JSON.parse(localStorage.getItem('user'));
        this.url = 'https://supertrade.supergrp.net:9085/rest/';
        this.Accounts = new Accounts(this.url, 1000,this.user);
    }

    componentWillMount() {
        setTimeout(() => {
            this.Accounts.GetAll().then((accounts) => {
                this.setState({ accounts })
            }).catch((error) => {
                console.log(error)
            })
        }, 2000);
    }

    onSearchClicked() {
        console.log(this.state.searchTerm);
        let prms = null;
        if (!this.state.searchTerm) {
            prms = this.Accounts.GetAll()
        } else {
            prms = this.Accounts.Search(this.state.searchTerm);
        }
        prms.then((accounts) => {
            this.setState({ accounts })
        }).catch((error) => {
            console.log(error)
        });
    }

    onSearchTermChanged(e) {
        this.setState({
            searchTerm: e.target.value
        });
    }


    render() {
        return (
            <Container>
                <div style={{ margin: 20 }}>
                    <Form>
                        <Input onChange={this.onSearchTermChanged.bind(this)} placeholder="Search for customer" />
                        <Button style={{ marginTop: 10 }} onClick={this.onSearchClicked.bind(this)} color="primary" size="lg" block>Search</Button>
                    </Form>
                    <ListGroup>

                        {this.state.accounts.map((o) => {
                            return (
                                <ListGroupItem>
                                    <Link to={`/account/${o.AccountID}|${o.BranchID}`}>
                                        {o.Name}
                                    </Link>
                                </ListGroupItem>
                            )
                        })}
                    </ListGroup>
                </div>
            </Container>
        );
    }
}

export default AccountList;
