import React, { Component } from 'react';
import { Redirect } from "react-router-dom";
import { Accounts, Pricelist } from 'rapidtradecore'
import { Container, ListGroup, ListGroupItem } from 'reactstrap';
import { Form, Input, Button } from 'reactstrap';

class PricelistSearch extends Component {
    constructor() {
        super();

        this.state = {
            account: null,
            searchResults: [],
            searchTerm: ""
        }
        this.url = 'https://supertrade.supergrp.net:9085/rest/';

        this.user = JSON.parse(localStorage.getItem('user'));
        this.Accounts = new Accounts(this.url, 1000,this.user);
        this.Pricelist = new Pricelist(this.url, 1000,this.user);
    }

    componentWillMount() {
        setTimeout(() => {
            this.Accounts.Get(this.props.match.params.id).then((result) => {
                let account = result;
                this.setState({ account });
                console.log(result);
            }).catch((error) => {
                console.log(error);
            });

            this.Pricelist.GetAll().then((results) => {
                console.log('fetched pricelists: ' + (results ? results.length || 0 : 0));
            })
        }, 2000)

    }

    onSearchClicked() {
        console.log(this.state.searchTerm);
        this.Pricelist.Search(this.state.searchTerm, this.state.account, false, '2',this.user, 0, 200).then((searchResults) => {
            this.setState({
                searchResults
            });
        }).catch((error) => {
            console.log(error);
        })
    }

    onSearchTermChanged(e) {
        this.setState({
            searchTerm: e.target.value
        });
    }

    render() {
        return (
            <Container>
                {
                    !this.state.account ? <h1> Loading .... </h1> :
                        <div style={{ margin: 20 }}>
                            <h6> Account : {this.state.account.Name} </h6>
                            <Form>
                                <Input onChange={this.onSearchTermChanged.bind(this)} placeholder="Search for products" />
                                <Button style={{ marginTop: 10 }} onClick={this.onSearchClicked.bind(this)} color="primary" size="lg" block>Search</Button>
                            </Form>
                            <ListGroup style={{marginTop : 20}}>
                                {this.state.searchResults.map((o) => {
                                    return (
                                        <ListGroupItem> {o.Description} </ListGroupItem>
                                    )
                                })}
                            </ListGroup>
                        </div>
                }
            </Container>
        );
    }
}

export default PricelistSearch;
