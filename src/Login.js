import React, { Component } from 'react';
import logo from './logo.svg';
import './Login.css';
import { Container, Navbar, NavbarBrand } from 'reactstrap';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Auth, Pricelist, Accounts, Stocks, DiscountConditions, DiscountValues, Discounts } from 'rapidtradecore';
import { Alert } from 'reactstrap';

class Login extends Component {
    constructor() {
        super();
        this.state = {
            password: "",
            userid: "",
            syncStatus : ""
        }

        this.auth = new Auth();
        this.url = 'https://supertrade.supergrp.net:9085/rest/';

        this.syncClasses = [
            Accounts,
            Pricelist,
            Stocks,
            DiscountConditions,
            DiscountValues,
            Discounts
        ]

        this.syncTables = [
            "Accounts",
            "Pricelist",
            "Stocks",
            "DiscountConditions",
            "DiscountValues",
            "Discounts"
        ]
        this.user = {};

    }
    onLoginUserIDChange(e) {
        this.setState({
            userid: e.target.value
        })
    }

    onLoginPasswordChange(e) {
        this.setState({
            password: e.target.value
        })
    }

    _recurseSync(idx, succ) {
        if (idx >= this.syncClasses.length) {
            succ();
            return;
        }

        this.setState({ syncStatus : this.syncTables[idx] });
        new this.syncClasses[idx](this.url, 1000,this.user).Sync((progress) => {
            console.log(`${this.syncTables[idx]} -- ${progress} `)
        }).then((msg) => {
            console.log(msg);
            this._recurseSync((idx + 1), succ);
        }).catch((err) => {
            console.log(err);
            this._recurseSync((idx + 1), succ);
        });
    }

    startSync() {
        return new Promise((res, rej) => {
            this._recurseSync(0, () => {
                res("---- All Syncs are done ----")
            });
        })
    }

    onSignInClicked() {
        //Set a session storage variable that shows I am signed in
        this.auth.VerifyUser(this.url, { userid: this.state.userid, password: this.state.password }).then((data) => {
            console.log(data);
            this.user = data;
            localStorage.setItem('user',JSON.stringify(data));
            return this.startSync(data)
        }).then((msg) => {
            console.log(msg)
            this.setState({ syncStatus : "---- All Syncs are done ----" });
            localStorage.setItem("loggedin", true)
        }).catch((err) => {
            alert(err)
        });
    }

    render() {
        return (
            <div>
                <Navbar dark style={{ backgroundColor: "#313740" }}>
                    <NavbarBrand href="/">Login</NavbarBrand>
                </Navbar>
                <Container>
                    <Form style={{ marginTop: 20 }}>
                        <FormGroup>
                            <Label for="userid">UserID</Label>
                            <Input onChange={this.onLoginUserIDChange.bind(this)} type="email" name="userid" id="login_userid" />
                        </FormGroup>

                        <FormGroup>
                            <Label for="password">Password</Label>
                            <Input onChange={this.onLoginPasswordChange.bind(this)} type="password" name="password" id="login_password" />
                        </FormGroup>
                    </Form>
                    <Button onClick={this.onSignInClicked.bind(this)} color="primary" size="lg" block>Sign In</Button>
                     { this.state.syncStatus != "" ? <Alert style={{ marginTop : 10 }} color="info"> Syncing : { this.state.syncStatus } </Alert> : null }
                </Container>
            </div>
        );
    }
}

export default Login;
